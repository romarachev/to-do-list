//
//  RealmProvider.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//

import Foundation
import XCTest
@testable import ToDoApp
import RealmSwift


struct RealmProvider {
    let configuration: Realm.Configuration
    
    internal init(config: Realm.Configuration) {
        configuration = config
    }
    
    var realm: Realm {
        return try! Realm(configuration: configuration)
    }
    
    private static let tasksConfig = Realm.Configuration(inMemoryIdentifier: "test", readOnly: false, objectTypes: [Task.self])
    
    public static var tasks: RealmProvider = {
        return RealmProvider(config: tasksConfig)
    }()
    
}

extension RealmProvider {
    func copyForTesting() -> RealmProvider {
        var conf = self.configuration
        conf.inMemoryIdentifier = "test"
        conf.readOnly = false
        return RealmProvider(config: conf)
    }
}


extension Realm {
    func addForTesting(objects: [Object]) {
        try! write {
            add(objects)
        }
    }
}
