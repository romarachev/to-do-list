//
//  MainWorkerTests.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//

import XCTest
@testable import ToDoApp
import RealmSwift

class MainServiceTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var sut: MainService!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMainService()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupMainService() {
        sut = MainService()
    }
    
    class MainServiceProtocolSpy: MainServiceProtocol {

        var receiveTasksFromRealmDataCalled = false
        var changeTaskStatusFromRealmCalled = false
        
        func receiveTasksFromRealmData() -> Results<Task> {
            receiveTasksFromRealmDataCalled = true
            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "test", readOnly: false))
            return realm.objects(Task.self).sorted(byKeyPath: "date")
        }
        
        func changeTaskStatusFromRealm(task: Task) {
            changeTaskStatusFromRealmCalled = true
            
            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "test", readOnly: false))
            
            try! realm.write {
                task.status.toggle()
            }
        }
    }
    
    
    
    // MARK: Tests
    
    func testReceiveTasksFromRealmData() {
        
        let spy = MainServiceProtocolSpy()
        
        let doneTask1 = Task(taskName: "task1", status: true)
        let doneTask2 = Task(taskName: "task2", status: false)
        let doneTask3 = Task(taskName: "task3", status: true)
        let doneTasks = [doneTask1, doneTask2, doneTask3]
        
        // Given
        let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "test", readOnly: false))
        try! realm.write {
            realm.add(doneTasks)
        }
        
        // When
        let receivedTasks = spy.receiveTasksFromRealmData()
        
        // Then
        XCTAssertEqual(doneTasks[0].taskName, receivedTasks[0].taskName, "\(receivedTasks) tasks should be the same")
        XCTAssertEqual(doneTasks.count, receivedTasks.count, "\(receivedTasks) displayTasks(viewModel:) should be the same")
        XCTAssertTrue(spy.receiveTasksFromRealmDataCalled, "")
    }
    
    
    func testChangeTaskFromRealm() {
        
        let spy = MainServiceProtocolSpy()
        
        // Given
        let taskToTogle = Task(taskName: "task1", status: false)
        
        // When
        spy.changeTaskStatusFromRealm(task: taskToTogle)
        
        // Then
        XCTAssertTrue(taskToTogle.status, "task status has changed")
        XCTAssertTrue(spy.changeTaskStatusFromRealmCalled, "")
    }
    
    
}
