//
//  MainViewControllerTests.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//

import XCTest
@testable import ToDoApp
import RealmSwift


class MainViewControllerTests: XCTestCase {
    
    // MARK: Subject under test
    
    var sut: MainViewController!
    var window: UIWindow!

    // MARK: Test lifecycle

    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupListTasksViewController()
    }

    override func tearDown() {
        window = nil
        super.tearDown()
    }

    // MARK: - Test setup
    
    func setupListTasksViewController() {
    
        sut = MainViewController()
        
    }

    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }

    // MARK: - Test doubles

    class MainBusinessLogicSpy: MainBusinessLogic {
        
        var receiveTasksCalled = false
        var selectTasksCalled = false
        var changeStatusCalled = false
        
        
        func receiveTasks(request: MainModel.ReceiveTasks.Request) {
            receiveTasksCalled = true
        }
        
        func selectTask(request: MainModel.SelectTask.Request) {
            selectTasksCalled = true
        }
        
        func changeStatus(request: MainModel.ChangeTaskStatus.Request) {
            changeStatusCalled = true
        }
        
    }
    

    func testShouldInitialLoadWhenViewIsLoaded() {
        // Given
        let spy = MainBusinessLogicSpy()
        sut.interactor = spy

        // When
        loadView()

        // Then
        XCTAssertTrue(spy.receiveTasksCalled, "viewDidLoad() should ask the interactor to do receiveTasks")
    }

    func testDisplayTasks() {
        let activeTask1 = Task(image: #imageLiteral(resourceName: "blank"), date: Date())
        let doneTask1 = Task(image: #imageLiteral(resourceName: "dots"), date: Date())
        let doneTask2 = Task(image: #imageLiteral(resourceName: "checkMark"), date: Date())
        
        // Given
        let viewModel = MainModel.ReceiveTasks.ViewModel(activeTasks: [activeTask1], doneTasks: [doneTask1, doneTask2])

        // When
        loadView()
        sut.displayAllTasks(viewModel: viewModel)
        

        // Then
        XCTAssertEqual(sut.activeTasks.count, viewModel.activeTasks.count, "displayTasks(viewModel:) should be the same")
    }
    
}

