//
//  MainInteractorTests.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//

import XCTest
@testable import ToDoApp
import RealmSwift

class MainInteractorTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var sut: MainInteractor!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMainInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }
    
    // MARK: - Test setup
    
    func setupMainInteractor() {
        sut = MainInteractor()
    }
    
    // MARK: - Test doubles
    
    class MainPresenterLogicSpy: MainPresenterLogic {
        
        var presentReceiveTasksCalled = false
        var selectTaskCalled = false
        var changeTaskStatusCalled = false
        
        func presentReceiveTasks(response: MainModel.ReceiveTasks.Response) {
            presentReceiveTasksCalled = true
        }
        
        func selectTask(response: MainModel.SelectTask.Response) {
            selectTaskCalled = true
        }
        
        func changeTaskStatus(response: MainModel.ChangeTaskStatus.Response) {
            changeTaskStatusCalled = true
        }
        
    }
    
    
    // MARK: - Tests
    
    func testPresentReceiveTasks() {
        
        // Given
        let spy = MainPresenterLogicSpy()
        sut.presenter = spy
        sut.service = MainServiceTests.MainServiceProtocolSpy()

        // When
        let request = MainModel.ReceiveTasks.Request()
        sut.receiveTasks(request: request)
        //sut.service = MainServiceProtocolSpy()

        // Then
        XCTAssert(spy.presentReceiveTasksCalled, "")
    }
    
    
    func testSelectTask() {
        // Given
        let spy = MainPresenterLogicSpy()
        sut.presenter = spy
        sut.service = MainServiceTests.MainServiceProtocolSpy()
        
        let task1 = Task(taskName: "foo1", status: false)
        let task2 = Task(taskName: "foo22", status: true)
        let tasks = [task1, task2]

        // When
        let request = MainModel.SelectTask.Request(indexPath: [0, 0], tasks: tasks)
        sut.selectTask(request: request)
        

        // Then
        XCTAssert(spy.selectTaskCalled, "")
        XCTAssertEqual("foo1", sut.selectedTask?.taskName)
    }
    
    
    func testChangeTaskStatus() {
        // Given
        let spy = MainPresenterLogicSpy()
        sut.presenter = spy
        sut.service = MainServiceTests.MainServiceProtocolSpy()
        
        let task1 = Task(taskName: "task1", status: false)

        // When
        let request = MainModel.ChangeTaskStatus.Request(task: task1)
        sut.changeStatus(request: request)
        
        // Then
        XCTAssert(spy.changeTaskStatusCalled, "")
    }
    
    
    
}
