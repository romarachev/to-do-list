//
//  MainPresenterTests.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//

import XCTest
@testable import ToDoApp
import RealmSwift

class MainPresenterTests: XCTest {
    
    // MARK: - Subject under test
    
    var sut: MainPresenter!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMainPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupMainPresenter() {
        sut = MainPresenter()
    }
    
    // MARK: - Test doubles
    
    class MainDisplayLogicSpy: MainDisplayLogic {
        
        var displayAllTasksCalled = false
        var selectTaskCalled = false
        var changeTaskStatusCalled = false
        
        func displayAllTasks(viewModel: MainModel.ReceiveTasks.ViewModel) {
            displayAllTasksCalled = true
        }
        
        
        func selectTask(viewModel: MainModel.SelectTask.ViewModel) {
            selectTaskCalled = true
        }
        
        func displayChangedTaskStatus(viewModel: MainModel.ChangeTaskStatus.ViewModel) {
            changeTaskStatusCalled = true
        }
    
    }
    
    // MARK: - Tests
    
    func testDisplayAllTasks() {
        // Given
        
        let tasks: Results<Task>?
        let spy = MainDisplayLogicSpy()
        sut.viewController = spy
        
        // When
        let response = MainModel.ReceiveTasks.Response(tasks: tasks)
        sut.presentReceiveTasks(response: response)
        
        // Then
        XCTAssertTrue(spy.displayAllTasksCalled, "presentReceiveTasks() asked the view controller to display the result")
    }
    
    
    func testSelectTask() {
        // Given
        let spy = MainDisplayLogicSpy()
        sut.viewController = spy
        
        let task1 = Task(taskName: "task1", status: false)
        let task2 = Task(taskName: "task2", status: true)
        let tasks = [task1, task2]
        
        // When
        let response = MainModel.SelectTask.Response(indexPath: [0, 1], tasks: tasks)
        sut.selectTask(response: response)
        
        // Then
        XCTAssertTrue(spy.selectTaskCalled, "selectTaskCalled() asked the view controller to go to the next screen")
    }
    
    
    func testChangeTestStatus() {
        // Given
        let spy = MainDisplayLogicSpy()
        sut.viewController = spy
        
        let task1 = Task(taskName: "task1", status: false)
        
        // When
        let response = MainModel.ChangeTaskStatus.Response(task: task1)
        sut.changeTaskStatus(response: response)
        
        // Then
        XCTAssertTrue(spy.changeTaskStatusCalled, "changeTaskStatusCalled() asked the view controller to change status")
        
    }
    
    
}
