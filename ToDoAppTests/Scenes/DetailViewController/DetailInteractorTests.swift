//
//  DetailInteractorTests.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//

import XCTest
@testable import ToDoApp
import RealmSwift

class DetailInteractorTests: XCTestCase {
    // MARK: Subject under test
    
    var sut: DetailInteractor!
    
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupDetailInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Test setup
    
    func setupDetailInteractor() {
        sut = DetailInteractor()
    }
    
    // MARK: Test doubles
    
    class DetailPresenterLogicSpy: DetailPresenterLogic {
        
        var presentTaskAddedCalled = false
        var presentUpdateFieldsIfTaskExistsCalled = false
        var presentUpdateTaskCalled = false
        var presentTaskDeletedCalled = false
        var presentUpdateImageCalled = false
        
        
        
        func presentTaskAdded(response: DetailModel.AddTask.Response) {
            presentTaskAddedCalled = true
        }
        
        func presentUpdateFieldsIfTaskExists(response: DetailModel.UpdateFieldsIfTaskExists.Response) {
            presentUpdateFieldsIfTaskExistsCalled = true
        }
        
        func presentUpdateTask(response: DetailModel.UpdateTask.Response) {
            presentUpdateTaskCalled = true
        }
        
        func presentTaskDeleted(response: DetailModel.DeleteTask.Response) {
            
//            guard let task = response.task else { return }
//            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "taskDelete", readOnly: false))
//            try! realm.write {
//                realm.delete(task)
//            }
            
            presentTaskDeletedCalled = true
        }
        
        func presentUpdateImage(response: DetailModel.UpdateImage.Response) {
            presentUpdateImageCalled = true
        }
        
    }
    
    
    // MARK: Tests
    
    func testPresentTaskAdded() {
        
        // Given
        let spy = DetailPresenterLogicSpy()
        sut.presenter = spy
        sut.service = DetailServiceTests.DetailServiceProtocolSpy()

        // When
    
        let taskField = DetailModel.AddTaskFields(name: "task1", description: "description", date: Date() as NSDate)
        let request = DetailModel.AddTask.Request(addTaskFields: taskField, errorMessage: nil)
        sut.addTask(request: request)
        

        // Then
        XCTAssert(spy.presentTaskAddedCalled, "presentTaskAdded() asked presenter to add task")
        
    }
    
    func testPresentUpdateFieldsIfTaskExists() {
        // Given
        let spy = DetailPresenterLogicSpy()
        sut.presenter = spy
        sut.service = DetailServiceTests.DetailServiceProtocolSpy()
        
        // When
        let request = DetailModel.UpdateFieldsIfTaskExists.Request()
        sut.updateFieldsIfTaskExists(request: request)
        
        // Then
        XCTAssert(spy.presentUpdateFieldsIfTaskExistsCalled, "updateFieldsIfTaskExist asked presenter to add field of task")
        
    }
    
    func testPresentUpdateTask() {
        // Given
        let spy = DetailPresenterLogicSpy()
        sut.presenter = spy
        sut.service = DetailServiceTests.DetailServiceProtocolSpy()
        sut.taskToUpdate = Task(taskName: "taskUpdate", status: true)
        
        // When
        //let task = Task(taskName: "taskUpdate", status: false)
        let task = DetailModel.AddTaskFields(name: "taskUpdate", description: "descriptionUpdate", date: Date() as NSDate)
        let request = DetailModel.UpdateTask.Request(task: task)
        sut.updateTask(request: request)
        
        // Then
        XCTAssert(spy.presentUpdateTaskCalled, "updateTask asked presenter to update task")
        
    }
    
    func testPresentTaskDeleted() {
        
        // Given
        let spy = DetailPresenterLogicSpy()
        sut.presenter = spy
        sut.service = DetailServiceTests.DetailServiceProtocolSpy()
        let task = Task(taskName: "taskDelete", status: true)
        sut.taskToUpdate = task
        sut.service.addTaskToRealm(taskName: task.taskName, taskDescription: task.taskDescription, date: task.date)
    
        // When
        let request = DetailModel.DeleteTask.Request()
        sut.deleteTask(request: request)

        // Then
        XCTAssert(spy.presentTaskDeletedCalled, "deletedTask() asked presenter to delete task")
        
    }
    
    func testPresentUpdateImage() {
        // Given
        let spy = DetailPresenterLogicSpy()
        sut.presenter = spy
        sut.service = DetailServiceTests.DetailServiceProtocolSpy()
        sut.taskToUpdate = Task(taskName: "taskUpdate", status: true)
        
        // When
        let imageData = #imageLiteral(resourceName: "plus").jpegData(compressionQuality: 0.7)
        let request = DetailModel.UpdateImage.Request(imageData: imageData as NSData?)
        sut.updateImage(request: request)
        
        // Then
        
        XCTAssert(spy.presentUpdateImageCalled, "updateImage() asked presenter to update task")
    }
    
    

    
    
    
}
