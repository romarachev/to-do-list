//
//  DetailPresenterControllerTests.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//


import XCTest
@testable import ToDoApp
import RealmSwift

class DetailPresenterTests: XCTest {
    
    // MARK: Subject under test

    var sut: DetailPresenter!

    // MARK: - Test lifecycle

    override func setUp() {
        super.setUp()
        setupDetailPresenter()
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: - Test setup

    func setupDetailPresenter() {
        sut = DetailPresenter()
    }

    // MARK: Test doubles

    class DetailDisplayLogicSpy: DetailDisplayLogic {
        
        var displayNewTaskAddedCalled = false
        var displayUpdateFieldsIfTaskExists = false
        var displayUpdateTaskCalled = false
        var displayTaskDeletedCalled = false
        var displayUpdateImageCalled = false
        
        
        func displayNewTaskAdded(viewModel: DetailModel.AddTask.ViewModel) {
            displayNewTaskAddedCalled = true
        }
        
        func displayUpdateFieldsIfTaskExists(viewModel: DetailModel.UpdateFieldsIfTaskExists.ViewModel) {
            displayUpdateFieldsIfTaskExists = true
        }
        
        func displayUpdateTask(viewModel: DetailModel.UpdateTask.ViewModel) {
            displayUpdateTaskCalled = true
        }
        
        func displayTaskDeleted(viewModel: DetailModel.DeleteTask.ViewModel) {
            displayTaskDeletedCalled = true
        }
        
        func displayUpdateImage(viewModel: DetailModel.UpdateImage.ViewModel) {
            displayUpdateImageCalled = true
        }
    
    }

    // MARK: - Tests
    
    func testDisplayNewTaskAdded() {
        // Given
        let spy = DetailDisplayLogicSpy()
        sut.viewController = spy

        // When
        
        let addTask = Task(taskName: "task1", status: false)
        let response = DetailModel.AddTask.Response(task: addTask, errorMessage: nil)
        sut.presentTaskAdded(response: response)
   
        // Then
        XCTAssertTrue(spy.displayNewTaskAddedCalled, "new task is added")
    }

    
    func testDisplayUpdateFieldsIfTaskExists() {
        // Given
        let spy = DetailDisplayLogicSpy()
        sut.viewController = spy

        // When
        
        let task = Task(taskName: "task2", status: false)
        let response = DetailModel.UpdateFieldsIfTaskExists.Response(task: task)
        sut.presentUpdateFieldsIfTaskExists(response: response)
        
        // Then
        XCTAssertTrue(spy.displayUpdateFieldsIfTaskExists, "new task is added")
    }

    
    func testDisplayUpdateTask() {
        // Given
        let spy = DetailDisplayLogicSpy()
        sut.viewController = spy

        // When
        let task = Task(taskName: "task3", status: false)
        let response = DetailModel.UpdateTask.Response(task: task)
        sut.presentUpdateTask(response: response)
   
        // Then
        XCTAssertTrue(spy.displayUpdateTaskCalled, "new task is added")
    }
    
    
    func testDisplayTaskDeleted() {
        // Given
        let spy = DetailDisplayLogicSpy()
        sut.viewController = spy

        // When
        let task = Task(taskName: "task4", status: false)
        let response = DetailModel.DeleteTask.Response(task: task, errorMessage: nil)
        sut.presentTaskDeleted(response: response)
   
        // Then
        XCTAssertTrue(spy.displayTaskDeletedCalled, "new task is added")
    }
    
    
    func testDisplayUpdateImage() {
        // Given
        let spy = DetailDisplayLogicSpy()
        sut.viewController = spy

        // When
        let task = Task(taskName: "task5", status: false)
        let response = DetailModel.UpdateImage.Response(task: task)
        sut.presentUpdateImage(response: response)
        
        // Then
        XCTAssertTrue(spy.displayUpdateImageCalled, "new task is added")
    }
    
    
}
