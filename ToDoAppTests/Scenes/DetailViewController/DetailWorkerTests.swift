//
//  DetailWorkerTests.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//

import XCTest
@testable import ToDoApp
import RealmSwift

class DetailServiceTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var sut: DetailService!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupDetailService()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupDetailService() {
        sut = DetailService()
    }
    
    class DetailServiceProtocolSpy: DetailServiceProtocol {
        
        var addTaskToRealmCalled = false
        var updateTaskInRealmCalled = false
        var deleteTaskFromRealmCalled = false
        var updateTaskImageCalled = false
        

        func addTaskToRealm(taskName: String, taskDescription: String, date: NSDate) {
            addTaskToRealmCalled = true
            
            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "taskAdded", readOnly: false))
            let task = Task()
            task.taskName = taskName
            task.taskDescription = taskDescription
            task.date = date
        
            try! realm.write {
                realm.add(task)
            }
        }
        
        
        func updateTaskInRealm(task: Task, taskName: String, taskDescription: String, date: NSDate) {
            updateTaskInRealmCalled = true
            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "taskUpdate", readOnly: false))
            try! realm.write {
                task.taskName = taskName
                task.taskDescription = taskDescription
                task.date = date
            }
        }
        
        func deleteTaskFromRealm(task: Task) {
            deleteTaskFromRealmCalled = true
//            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "taskDelete", readOnly: false))
//            try! realm.write {
//                realm.delete(task)
//            }
        }
        
        func updateTaskImage(task: Task, imageData: NSData) {
            updateTaskImageCalled = true
            
            let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "imageUpdate", readOnly: false))
            
            try! realm.write {
                task.imageData = imageData
            }
        }
        
    }
    
    
    
    // MARK: - Tests
    
    func testAddTaskToRealm() {
        
        // Given
        let spy = DetailServiceProtocolSpy()
        
        // When
        spy.addTaskToRealm(taskName: "addTask1", taskDescription: "addTaskDescription1", date: Date() as NSDate)
        spy.addTaskToRealm(taskName: "addTask2", taskDescription: "addTaskDescription2", date: Date() as NSDate)
        let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "taskAdded", readOnly: false))
        let taskNumberFromRealm = realm.objects(Task.self)
        
        // Then
        XCTAssertTrue(spy.addTaskToRealmCalled, "task added to realm")
        XCTAssertEqual(2, taskNumberFromRealm.count, "number of tasks equal")
    }
    
    
    func testUpdateTaskInRealm() {
        // Given
        let spy = DetailServiceProtocolSpy()
        let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "taskUpdate", readOnly: false))
        let taskToUpdate = Task(taskName: "nameBeforeUpdate", status: false)
        try! realm.write {
            realm.add(taskToUpdate)
        }
        
        // When
        spy.updateTaskInRealm(task: taskToUpdate, taskName: "nameAfterUpdate", taskDescription: "addition", date: Date() as NSDate)
        
        // Then
        XCTAssertTrue(spy.updateTaskInRealmCalled, "update is successful")
        XCTAssertEqual("nameAfterUpdate", taskToUpdate.taskName, "name is correct")
        XCTAssertEqual("addition", taskToUpdate.taskDescription, "description is correct")
    }
    
    
    func testDeleteTaskInRealm() {
        // Given
        let spy = DetailServiceProtocolSpy()
        let taskToDelete = Task(taskName: "nameBeforeUpdate", status: false)

        // When
        spy.deleteTaskFromRealm(task: taskToDelete)
       
        // Then
        XCTAssertTrue(spy.deleteTaskFromRealmCalled, "delete is succeessful")
    }
    
    
    func testUpdateTaskImageInRealm() {
        // Given
        let spy = DetailServiceProtocolSpy()
        let realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "imageUpdate", readOnly: false))
        let taskToUpdate = Task(image: #imageLiteral(resourceName: "plus"), date: Date())
        
        try! realm.write {
            realm.add(taskToUpdate)
        }

        // When
        let dataImage = #imageLiteral(resourceName: "checkMark").jpegData(compressionQuality: 0.7)! as NSData
        spy.updateTaskImage(task: taskToUpdate, imageData: dataImage)
        
        // Then
        XCTAssertTrue(spy.updateTaskImageCalled)
        XCTAssertEqual(dataImage, taskToUpdate.imageData)
    }
    
    
}
