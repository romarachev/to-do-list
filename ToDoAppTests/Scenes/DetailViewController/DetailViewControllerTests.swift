//
//  DetaiViewControllerTests.swift
//  ToDoAppTests
//
//  Created by Роман Рачев on 08.02.2021.
//

import XCTest
@testable import ToDoApp


class DetailViewControllerTests: XCTestCase {
    
    // MARK: Subject under test
    
    var sut: DetailViewController!
    var window: UIWindow!

    // MARK: Test lifecycle

    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupDetailViewController()
    }

    override func tearDown() {
        window = nil
        super.tearDown()
    }

    // MARK: Test setup

    func setupDetailViewController() {
        
        sut = DetailViewController()
    }

    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }

    // MARK: Test doubles

    class DetailBusinessLogicSpy: DetailBusinessLogic {
        
        var addTaskCalled = false
        var updateFieldsIfTaskExistsCalled = false
        var updateTaskCalled = false
        var deleteTaskCalled = false
        var updateImageCalled = false
        
        
        func addTask(request: DetailModel.AddTask.Request) {
            addTaskCalled = true
        }
        
        func updateFieldsIfTaskExists(request: DetailModel.UpdateFieldsIfTaskExists.Request) {
            updateFieldsIfTaskExistsCalled = true
        }
        
        func updateTask(request: DetailModel.UpdateTask.Request) {
            updateTaskCalled = true
        }
        
        func deleteTask(request: DetailModel.DeleteTask.Request) {
            deleteTaskCalled = true
        }
        
        func updateImage(request: DetailModel.UpdateImage.Request) {
            updateImageCalled = true
        }
        

    }

    // MARK: Tests

    func testInitialLoadWhenViewIsLoaded() {
        // Given
        let spy = DetailBusinessLogicSpy()
        sut.interactor = spy

        // When
        loadView()

        // Then
        XCTAssertTrue(spy.updateFieldsIfTaskExistsCalled, "viewDidLoad() should ask the interactor to update task if exist")
    }
    
    
    func testAddTask() {
        // Given
        let spy = DetailBusinessLogicSpy()
        sut.interactor = spy
        
        
        // When
        sut.addTask()
        
        // Then
        XCTAssertTrue(spy.addTaskCalled, "addTask() asked the interactor to add task")
        
    }
    
    func testUpdateTask() {
        // Given
        let spy = DetailBusinessLogicSpy()
        sut.interactor = spy
        
        
        // When
        sut.updateTask()
        
        // Then
        XCTAssertTrue(spy.updateTaskCalled, "updateTask() asked the interactor to update task")
    }
}
