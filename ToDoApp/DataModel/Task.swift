//
//  Task.swift
//  ToDoApp
//
//  Created by Роман Рачев on 05.02.2021.
//

import Foundation
import RealmSwift

class Task: Object {

    // MARK: - properties
    
    @objc dynamic var taskName = ""
    @objc dynamic var taskDescription = ""
    @objc dynamic var status = false
    @objc dynamic var imageData = NSData()
    @objc dynamic var date = Date.init()
    
    // MARK: - init
    
    convenience init(image: UIImage, date: Date) {
        self.init()
        
        if let dataImage = image.jpegData(compressionQuality: 0.7) {
            self.imageData = dataImage as NSData
        }
        self.date = date as Date
    }
    
    
    convenience init(taskName: String, status: Bool) {
        self.init()
    
        self.taskName = taskName
        self.status = status
        
    }
    

}


