//
//  Realm.swift
//  ToDoApp
//
//  Created by Роман Рачев on 14.05.2021.
//

import RealmSwift

extension Realm {
    
    
    static func safeInit() -> Realm? {
        do {
            let realm = try Realm()
            return realm
        }
        catch {
            // LOG ERROR
        }
        return nil
    }

    
    func safeWrite(_ block: () -> ()) {
        do {
            // Async safety, to prevent "Realm already in a write transaction" Exceptions
            if !isInWriteTransaction {
                try write(block)
            }
        } catch {
            // LOG ERROR
        }
    }
    
    
}
