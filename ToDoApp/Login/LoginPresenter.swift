//
//  LoginPresenter.swift
//  ToDoApp
//
//  Created by Роман Рачев on 05.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol LoginPresentationLogic {
    func presentData(response: Login.Model.Response.ResponseType)
}


class LoginPresenter: LoginPresentationLogic {
    
    
    weak var viewController: LoginDisplayLogic?
    
    
    func presentData(response: Login.Model.Response.ResponseType) {
        
    }
    
    
}
