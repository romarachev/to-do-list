//
//  LoginViewController.swift
//  ToDoApp
//
//  Created by Роман Рачев on 05.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SnapKit


protocol LoginDisplayLogic: AnyObject {
    func displayData(viewModel: Login.Model.ViewModel.ViewModelData)
}


class LoginViewController: UIViewController, LoginDisplayLogic {
    
    // MARK: - properties
    
    var interactor: LoginBusinessLogic?
    var router: (NSObjectProtocol & LoginRouterInput)?
    
    // MARK: - views
    
    lazy var nameTextField: UITextField = {
        let textField = UITextField()
        textField.layer.borderWidth = 2
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.cornerRadius = 4
        textField.layer.masksToBounds = true
        textField.setLeftPaddingPoints(10)
        textField.placeholder = "Write login"
        return textField
    }()
    
    
    lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.layer.borderWidth = 2
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.cornerRadius = 4
        textField.layer.masksToBounds = true
        textField.setLeftPaddingPoints(10)
        textField.placeholder = "Write password"
        return textField
    }()
    
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("Log in", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .lightGray
        button.titleLabel?.font = .boldSystemFont(ofSize: 20)
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(loginToApp), for: .touchUpInside)
        return button
    }()

    // MARK: - setup
    
    private func setup() {
        let viewController        = self
        let interactor            = LoginInteractor()
        let presenter             = LoginPresenter()
        let router                = LoginRouter(viewController: self)
        viewController.interactor = interactor
        viewController.router     = router
        interactor.presenter      = presenter
        presenter.viewController  = viewController
        router.viewController     = viewController
    }
    
    // MARK: - lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setup()
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(nameTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        
        setupConstraintForNameTextField()
        setupConstraintForPasswordTextField()
        setupConstraintForLoginButton()
        
    }
    
    // MARK: - methods
    
    func displayData(viewModel: Login.Model.ViewModel.ViewModelData) {
        
    }
    
    
    @objc func loginToApp() {
        router?.navigateToPushedViewController()
    }
    
    
}

// MARK: - sizes extension

extension LoginViewController {
    
    
    func setupConstraintForNameTextField() {
        nameTextField.snp.makeConstraints { make in
            make.top.equalTo(view.snp.centerY)
            make.left.equalTo(view.snp.left).offset(32)
            make.right.equalTo(view.snp.right).offset(-32)
            make.height.equalTo(50)
        }
    }
    
    
    func setupConstraintForPasswordTextField() {
        passwordTextField.snp.makeConstraints { make in
            make.top.equalTo(nameTextField.snp.bottom).offset(16)
            make.left.equalTo(view.snp.left).offset(32)
            make.right.equalTo(view.snp.right).offset(-32)
            make.height.equalTo(50)
        }
    }
    
    
    func setupConstraintForLoginButton() {
        loginButton.snp.makeConstraints { make in
            make.top.equalTo(passwordTextField.snp.bottom).offset(16)
            make.left.equalTo(view.snp.left).offset(32)
            make.right.equalTo(view.snp.right).offset(-32)
            make.height.equalTo(50)
        }
    }
    
    
}
