//
//  LoginRouter.swift
//  ToDoApp
//
//  Created by Роман Рачев on 05.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit


protocol LoginRouterInput {
    func navigateToPushedViewController()
}


class LoginRouter: NSObject, LoginRouterInput {
    
    // MARK: - properties
    
    weak var viewController: LoginViewController?
    
    // MARK: - init
    
    init(viewController: LoginViewController) {
        self.viewController = viewController
    }
    
    // MARK: - methods
    
    func navigateToPushedViewController() {
        UserDefaults.standard.set(true, forKey: "isUserSignIn")
        let pushedViewController = MainViewController()
        let navController = UINavigationController(rootViewController: pushedViewController)
        navController.modalPresentationStyle = .fullScreen
        viewController?.present(navController, animated: true, completion: nil)
    }
    
    
}
