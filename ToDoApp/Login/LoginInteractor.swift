//
//  LoginInteractor.swift
//  ToDoApp
//
//  Created by Роман Рачев on 05.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit


protocol LoginBusinessLogic {
    func makeRequest(request: Login.Model.Request.RequestType)
}


class LoginInteractor: LoginBusinessLogic {
    
    // MARK: - properties
    
    var presenter: LoginPresentationLogic?
    var service: LoginService?
    
    // MARK: - methods
    
    func makeRequest(request: Login.Model.Request.RequestType) {
        if service == nil {
            service = LoginService()
        }
    }
    
    
}
