//
//  MainWorker.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import RealmSwift


protocol MainServiceProtocol {
    func receiveTasksFromRealmData() -> Results<Task>
    func changeTaskStatusFromRealm(task: Task)
}

class MainService: MainServiceProtocol {
    
    // MARK: - methods
    
    func receiveTasksFromRealmData() -> Results<Task> {
        let realm = try! Realm()
        return realm.objects(Task.self).sorted(byKeyPath: "date")
    }
    
    
    func changeTaskStatusFromRealm(task: Task) {
        if let realm = Realm.safeInit() {
            realm.safeWrite {
                task.status.toggle()
            }
        }
    }
    
    
}
