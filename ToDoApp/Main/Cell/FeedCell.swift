//
//  UserHeader.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//

import Foundation
import UIKit


protocol ChangeStatusTaskCellDelegate: AnyObject {
    func changeStatusTask(_ feedCell: FeedCell, buttonTappedFor task: Task)
}


class FeedCell: UICollectionViewCell {
    
    // MARK: - properties
    
    weak var delegate: ChangeStatusTaskCellDelegate?
    var task: Task?
    
    // MARK: - views
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 4
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    
    lazy var button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.layer.cornerRadius = 2
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        return button
    }()
    
    
    lazy var taskLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 18)
        label.textAlignment = .center
        label.textColor = .black
        label.textAlignment = .left
        label.preferredMaxLayoutWidth = 300
        return label
    }()
    
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 18)
        label.textAlignment = .center
        label.textColor = .gray
        label.textAlignment = .left
        label.preferredMaxLayoutWidth = 300
        return label
    }()
    
    // MARK: - init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(button)
        addSubview(taskLabel)
        addSubview(descriptionLabel)
        addSubview(imageView)
        
        setupConstraintsForButton()
        setupConstraintsForTaskLabel()
        setupConstraintsForDescriptionLabel()
        setupConstraintsForImageView()
        
        button.addTarget(self, action: #selector(taskButtonTouched), for: .touchUpInside)
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - methods
    
    
    func getFormattedDate(date: Date, format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: date)
    }
    
    
    func set(viewModel: Task) {
        taskLabel.text = viewModel.taskName
        descriptionLabel.text = getFormattedDate(date: viewModel.date, format: "dd-MMM-yyyy")
        imageView.image = UIImage(data: viewModel.imageData as Data)
        task = viewModel
    }
    
    
    @objc func taskButtonTouched(sender: UIButton!) {
        if let delegate = delegate, let task = task {
            delegate.changeStatusTask(self, buttonTappedFor: task)
        }
    }
    
    
}

// MARK: - sizes extension

extension FeedCell {
    
    
    func setupConstraintsForButton() {
        button.snp.makeConstraints { make in
            make.top.equalTo(snp.top).offset(12)
            make.left.equalTo(snp.left).offset(10)
            make.width.equalTo(15)
            make.height.equalTo(15)
        }
    }
    
    
    func setupConstraintsForTaskLabel() {
        taskLabel.snp.makeConstraints { make in
            make.top.equalTo(snp.top).offset(10)
            make.left.equalTo(button.snp.right).offset(10)
        }
    }
    
    
    func setupConstraintsForDescriptionLabel() {
        descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(taskLabel.snp.bottom).offset(10)
            make.left.equalTo(button.snp.right).offset(10)
        }
    }
    
    
    func setupConstraintsForImageView() {
        imageView.snp.makeConstraints { make in
            make.top.equalTo(snp.top).offset(5)
            make.right.equalTo(snp.right).offset(-10)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
    }
    
    
}
