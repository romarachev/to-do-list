//
//  MainHeader.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//

import Foundation
import UIKit


class MainHeader: UICollectionViewCell {
    
    // MARK: - views
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.textColor = .black
        return label
    }()
    
    // MARK: - init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .mainColor()
        addSubview(nameLabel)
        setupConstraintsForNameLabel()

    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}

// MARK: - sizes extension

extension MainHeader {
    
    
    func setupConstraintsForNameLabel() {
        nameLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    
}
