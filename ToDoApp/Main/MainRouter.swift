//
//  MainRouter.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MainRoutingLogic {
    func routeToDetail()
}


protocol MainDataPassing {
    var dataStore: MainTasksDataStore? { get }
}


class MainRouter: NSObject, MainRoutingLogic, MainDataPassing {
    
    // MARK: - properties
    
    weak var viewController: MainViewController?
    let destinationVC = DetailViewController()
    var dataStore: MainTasksDataStore?
    
    // MARK: - methods
    
    func routeToDetail() {
        guard let viewController = viewController,
              let sourceData = dataStore,
              var destinationData = destinationVC.router?.dataStore
              else { return }

        passDataToDetail(source: sourceData, destination: &destinationData)
        navigateToDetail(source: viewController, destination: destinationVC)
    }

    
    private func passDataToDetail(source: MainTasksDataStore, destination: inout DetailTasksDataStore) {
        destination.taskToUpdate = source.selectedTask
    }
    
    
    private func navigateToDetail(source: MainViewController, destination: DetailViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }
    
    
}
