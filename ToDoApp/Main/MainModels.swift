//
//  MainModels.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import RealmSwift

enum MainModel {
   
    
    enum ReceiveTasks {
        struct Request {
        }
        
        struct Response {
            var tasks: Results<Task>?
        }
        
        struct ViewModel {
            var activeTasks = [Task]()
            var doneTasks = [Task]()
        }
    }
    
    
    enum SelectTask {
        struct Request {
            var indexPath: IndexPath
            var tasks = [Task]()
        }
        
        struct Response {
            var indexPath: IndexPath
            var tasks = [Task]()
        }
        
        struct ViewModel {
            var indexPath: IndexPath
            var tasks = [Task]()
        }
    }
    
    enum ChangeTaskStatus {
        struct Request {
            var task: Task?
        }
        
        struct Response {
            var task: Task?
        }
        
        struct ViewModel {
            var task: Task?
        }
    }
    
    
}
