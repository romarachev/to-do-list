//
//  MainInteractor.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import RealmSwift

protocol MainBusinessLogic {
    
    func receiveTasks(request: MainModel.ReceiveTasks.Request)
    func selectTask(request: MainModel.SelectTask.Request)
    func changeStatus(request: MainModel.ChangeTaskStatus.Request)
    
}

protocol MainTasksDataStore {
    var allTasks: Results<Task>? { get set }
    var selectedTask: Task? { get set }
}

class MainInteractor: MainBusinessLogic, MainTasksDataStore {
    
    // MARK: - properties
    
    var service: MainServiceProtocol = MainService()
    var presenter: MainPresenterLogic?
    var allTasks: Results<Task>?
    var selectedTask: Task?
    
    // MARK: - methods
    
    func receiveTasks(request: MainModel.ReceiveTasks.Request) {
        let tasks = service.receiveTasksFromRealmData()
        allTasks = tasks
        let response = MainModel.ReceiveTasks.Response(tasks: tasks)
        presenter?.presentReceiveTasks(response: response)
    }
    
    
    func selectTask(request: MainModel.SelectTask.Request) {
        let task = request.tasks[request.indexPath.row]
        selectedTask = task
        let response = MainModel.SelectTask.Response(indexPath: request.indexPath)
        presenter?.selectTask(response: response)
    }
    
    
    func changeStatus(request: MainModel.ChangeTaskStatus.Request) {
        guard let taskToTogle = request.task else { return }
        service.changeTaskStatusFromRealm(task: taskToTogle)
        let response = MainModel.ChangeTaskStatus.Response(task: request.task)
        presenter?.changeTaskStatus(response: response)
    }
    
    
}
