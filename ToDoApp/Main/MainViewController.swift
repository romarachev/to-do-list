//
//  MainViewController.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit


protocol MainDisplayLogic: AnyObject {
    func displayAllTasks(viewModel: MainModel.ReceiveTasks.ViewModel)
    func selectTask(viewModel: MainModel.SelectTask.ViewModel)
    func displayChangedTaskStatus(viewModel: MainModel.ChangeTaskStatus.ViewModel)
}


class MainViewController: UICollectionViewController,  MainDisplayLogic {

    // MARK: - init

    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - properties
    
    var interactor: MainBusinessLogic?
    var router: (NSObjectProtocol & MainRoutingLogic & MainDataPassing)?
    var doneTasks = [Task]()
    var activeTasks = [Task]()
    
    // MARK: - setup
    
    private func setup() {
        let viewController        = self
        let interactor            = MainInteractor()
        let presenter             = MainPresenter()
        let router                = MainRouter()
        viewController.interactor = interactor
        viewController.router     = router
        interactor.presenter      = presenter
        presenter.viewController  = viewController
        router.viewController     = viewController
        router.dataStore          = interactor
    }
    
    // MARK: -lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setup()
        receiveTasks()
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        title = "To Do app"
        navigationItem.largeTitleDisplayMode = .always
        collectionView.register(FeedCell.self, forCellWithReuseIdentifier: "FeedCell")
        collectionView.register(MainHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "MainHeader")
        collectionView.backgroundColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "plus"), style: .done, target: self, action: #selector(addNewTask))
        navigationController?.navigationBar.tintColor = .black
        
    }
    
    // MARK: - methods
    
    @objc func addNewTask() {
        router?.routeToDetail()
    }
    
    
    func receiveTasks() {
        let request = MainModel.ReceiveTasks.Request()
        interactor?.receiveTasks(request: request)
    }
    
    
    func displayAllTasks(viewModel: MainModel.ReceiveTasks.ViewModel) {
        activeTasks = viewModel.activeTasks
        doneTasks = viewModel.doneTasks
        collectionView.reloadData()
    }
    
    
    func selectTask(viewModel: MainModel.SelectTask.ViewModel) {
        router?.routeToDetail()
    }
    
    
    func displayChangedTaskStatus(viewModel: MainModel.ChangeTaskStatus.ViewModel) {
        
        guard let task = viewModel.task else { return }
        if !task.status {
            activeTasks.append(task)
        } else {
            doneTasks.append(task)
        }
        collectionView.reloadData()
    }
    
    
}

// MARK: - ChangeStatusTaskCellDelegate

extension MainViewController: ChangeStatusTaskCellDelegate {

    
    func changeStatusTask(_ feedCell: FeedCell, buttonTappedFor task: Task) {
        if !task.status {
            activeTasks = activeTasks.filter  { $0 != task }
        } else {
            doneTasks = doneTasks.filter  { $0 != task }
        }
        let request = MainModel.ChangeTaskStatus.Request(task: task)
        interactor?.changeStatus(request: request)
    }


}

// MARK: - UICollectionViewDelegateFlowLayout

extension MainViewController:  UICollectionViewDelegateFlowLayout {
    
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MainHeader", for: indexPath) as! MainHeader
            if indexPath.section == 0 {
                header.nameLabel.text = "New tasks"
                return header
            } else {
                header.nameLabel.text = "Done tasks"
                return header
            }
        default:
            assert(false, "Unexpected element kind")
        }
        return UICollectionReusableView()
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width, height: 70)
    }

    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCell", for: indexPath) as! FeedCell
        cell.layer.addBorder(edge: .bottom, color: .lightGray, thickness: 0.4)
        cell.delegate = self
        
        if indexPath.section == 0 {
            cell.set(viewModel: activeTasks[indexPath.item])
            cell.button.setBackgroundImage(#imageLiteral(resourceName: "blank"), for: .normal)
        } else if indexPath.section == 1 {
            cell.set(viewModel: doneTasks[indexPath.item])
            cell.button.setBackgroundImage(#imageLiteral(resourceName: "checkMark"), for: .normal)
        }
        return cell
    }

    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let request = MainModel.SelectTask.Request(indexPath: indexPath, tasks: activeTasks)
            interactor?.selectTask(request: request)
        } else if indexPath.section == 1 {
            let request = MainModel.SelectTask.Request(indexPath: indexPath, tasks: doneTasks)
            interactor?.selectTask(request: request)
        }
    }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return activeTasks.count
        } else {
            return doneTasks.count
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: 80)
    }
    
    
}
