//
//  MainPresenter.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MainPresenterLogic {
    func presentReceiveTasks(response: MainModel.ReceiveTasks.Response)
    func selectTask(response: MainModel.SelectTask.Response)
    func changeTaskStatus(response: MainModel.ChangeTaskStatus.Response)
}

class MainPresenter: MainPresenterLogic {
    
    // MARK: - properties
    
    weak var viewController: MainDisplayLogic?
    var activeTasks = [Task]()
    var doneTasks = [Task]()
    
    // MARK: - methods
    
    func presentReceiveTasks(response: MainModel.ReceiveTasks.Response) {
        guard let tasks = response.tasks else { return }
        
        for task in tasks {
            if task.status == true {
                doneTasks.append(task)
            } else {
                activeTasks.append(task)
            }
        }
        
        let viewModel = MainModel.ReceiveTasks.ViewModel(activeTasks: activeTasks, doneTasks: doneTasks)
        viewController?.displayAllTasks(viewModel: viewModel)
    }
    
    
    func selectTask(response: MainModel.SelectTask.Response) {
        let viewModel = MainModel.SelectTask.ViewModel(indexPath: response.indexPath)
        viewController?.selectTask(viewModel: viewModel)
    }
    
    
    func changeTaskStatus(response: MainModel.ChangeTaskStatus.Response) {
        
        let viewModel = MainModel.ChangeTaskStatus.ViewModel(task: response.task)
        viewController?.displayChangedTaskStatus(viewModel: viewModel)
    }
    
    
}
