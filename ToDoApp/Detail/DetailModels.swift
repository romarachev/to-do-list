//
//  DetailModels.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

enum DetailModel {
    
    
    struct AddTaskFields {
        var name: String
        var description: String
        var date: NSDate
    }
    
    
    enum AddTask {
        struct Request {
            var addTaskFields: AddTaskFields?
            var errorMessage: String?
        }
        
        struct Response {
            var task: Task?
            var errorMessage: String?
        }
        
        struct ViewModel {
            var task: Task?
            var errorMessage: String?
        }
    }
    
    
    enum UpdateFieldsIfTaskExists {
        struct Request {
        }
        
        struct Response {
            var task: Task?
        }
        
        struct ViewModel {
            var task: Task?
        }
    }

    
    enum UpdateTask {
        struct Request {
            var task: AddTaskFields
        }
        
        struct Response {
            var task: Task?
        }
        
        struct ViewModel {
            var task: Task?
        }
    }
    
    
    enum DeleteTask {
        struct Request {
            var task: Task?
            var errorMessage: String?
        }
        
        struct Response {
            var task: Task?
            var errorMessage: String?
        }
        
        struct ViewModel {
            var task: Task?
            var errorMessage: String?
        }
    }
    
    
    enum UpdateImage {
        struct Request {
            var imageData: NSData?
        }
        
        struct Response {
            var task: Task?
        }
        
        struct ViewModel {
            var task: Task?
        }
    }
    
    
    enum DeleteImage {
        
        struct Request {
            var errorMessage: String?
        }
        
        struct Response {
            var task: Task?
            var errorMessage: String?
        }
        
        struct ViewModel {
            var task: Task?
            var errorMessage: String?
        }
    }
    
    
}
