//
//  DetailWorker.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import RealmSwift


protocol DetailServiceProtocol {
    func addTaskToRealm(taskName: String, taskDescription: String, date: Date)
    func updateTaskInRealm(task: Task, taskName: String, taskDescription: String, date: Date)
    func deleteTaskFromRealm(task: Task)
    func updateTaskImage(task: Task, imageData: NSData)
}


class DetailService: DetailServiceProtocol  {
    
    // MARK: - methods
    
    func addTaskToRealm(taskName: String, taskDescription: String, date: Date) {
        
        let task = Task()
        task.taskName = taskName
        task.taskDescription = taskDescription
        task.date = date
    
        if let realm = Realm.safeInit() {
            realm.safeWrite {
                realm.add(task)
            }
        }
    }

    func updateTaskInRealm(task: Task, taskName: String, taskDescription: String, date: Date) {
        if let realm = Realm.safeInit() {
            realm.safeWrite {
                task.taskName = taskName
                task.taskDescription = taskDescription
                task.date = date
            }
        }
    }
    
    func deleteTaskFromRealm(task: Task) {
        if let realm = Realm.safeInit() {
            realm.safeWrite {
                realm.delete(task)
            }
        }
    }
    
    
    func updateTaskImage(task: Task, imageData: NSData) {
        if let realm = Realm.safeInit() {
            realm.safeWrite {
                task.imageData = imageData
            }
        }
    }
    
  
}
