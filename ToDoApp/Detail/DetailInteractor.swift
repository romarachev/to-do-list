//
//  DetailInteractor.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DetailBusinessLogic {
    func addTask(request: DetailModel.AddTask.Request)
    func updateFieldsIfTaskExists(request: DetailModel.UpdateFieldsIfTaskExists.Request)
    func updateTask(request: DetailModel.UpdateTask.Request)
    func deleteTask(request: DetailModel.DeleteTask.Request)
    func updateImage(request: DetailModel.UpdateImage.Request)
}


protocol DetailTasksDataStore {
    var taskToUpdate: Task? { get set }
}


class DetailInteractor: DetailBusinessLogic, DetailTasksDataStore {

    // MARK: - properties
    
    var presenter: DetailPresenterLogic?
    var service: DetailServiceProtocol = DetailService()
    var taskToUpdate: Task?
    
    
    // MARK: - methods
    
    func addTask(request: DetailModel.AddTask.Request) {
        if request.errorMessage != nil {
            let response = DetailModel.AddTask.Response(task: nil, errorMessage: request.errorMessage)
            presenter?.presentTaskAdded(response: response)
            return
        }
        
        guard let addTaskFields = request.addTaskFields else { return }
        service.addTaskToRealm(taskName: addTaskFields.name, taskDescription: addTaskFields.description, date: Date())
        let response = DetailModel.AddTask.Response()
        presenter?.presentTaskAdded(response: response)
    }
    
    
    func deleteTask(request: DetailModel.DeleteTask.Request) {
        
        guard let taskToDelete = taskToUpdate else { return }
        service.deleteTaskFromRealm(task: taskToDelete)
        let response = DetailModel.DeleteTask.Response()
        presenter?.presentTaskDeleted(response: response)
    }
    
    
    func updateFieldsIfTaskExists(request: DetailModel.UpdateFieldsIfTaskExists.Request) {
        if taskToUpdate != nil {
            let response = DetailModel.UpdateFieldsIfTaskExists.Response(task: taskToUpdate)
            presenter?.presentUpdateFieldsIfTaskExists(response: response)
        } else {
            let response = DetailModel.UpdateFieldsIfTaskExists.Response(task: nil)
            presenter?.presentUpdateFieldsIfTaskExists(response: response)
        }
    }
    
    
    func updateTask(request: DetailModel.UpdateTask.Request) {
        guard let taskToUpdate = taskToUpdate else { return }
        service.updateTaskInRealm(task: taskToUpdate, taskName: request.task.name, taskDescription: request.task.description, date: Date())
        let response = DetailModel.UpdateTask.Response(task: taskToUpdate)
        presenter?.presentUpdateTask(response: response)
    }
    
    
    func updateImage(request: DetailModel.UpdateImage.Request) {
        if let taskToUpdate = taskToUpdate,
           let imageData = request.imageData {
            service.updateTaskImage(task: taskToUpdate, imageData: imageData)
            let response = DetailModel.UpdateImage.Response(task: taskToUpdate)
            presenter?.presentUpdateImage(response: response)
        }
    }
    
    
}
