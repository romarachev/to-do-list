//
//  DetailRouter.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DetailRoutingLogic {
    
}

protocol DetailDataPassing {
    var dataStore: DetailTasksDataStore? { get }
}



class DetailRouter: NSObject, DetailRoutingLogic, DetailDataPassing {
    
    // MARK: - properties
    
    weak var viewController: DetailViewController?
    var dataStore: DetailTasksDataStore?
    
}
