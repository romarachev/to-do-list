//
//  DetailViewController.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DetailDisplayLogic: AnyObject {
    func displayNewTaskAdded(viewModel: DetailModel.AddTask.ViewModel)
    func displayUpdateFieldsIfTaskExists(viewModel: DetailModel.UpdateFieldsIfTaskExists.ViewModel)
    func displayUpdateTask(viewModel: DetailModel.UpdateTask.ViewModel)
    func displayTaskDeleted(viewModel: DetailModel.DeleteTask.ViewModel)
    func displayUpdateImage(viewModel: DetailModel.UpdateImage.ViewModel)
}


class DetailViewController: UIViewController, DetailDisplayLogic {

    // MARK: - init
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - properties
    
    var interactor: DetailBusinessLogic?
    var router: (NSObjectProtocol & DetailRoutingLogic & DetailDataPassing)?
    var task: Task?
    
    // MARK: - views
    
    lazy var taskTextField: UITextField = {
        let textField = UITextField()
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 4
        textField.layer.masksToBounds = true
        textField.placeholder = "Write name"
        textField.font = .boldSystemFont(ofSize: 14)
        textField.setLeftPaddingPoints(10)
        return textField
    }()
    
    
    lazy var descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.layer.borderWidth = 1
        textView.layer.cornerRadius = 4
        textView.layer.masksToBounds = true
        textView.font = .systemFont(ofSize: 14)
        return textView
    }()
    
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .mainColor()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 4
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    
    lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("Save task", for: .normal)
        button.backgroundColor = .lightGray
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(saveTask), for: .touchUpInside)
        return button
    }()
    
    
    lazy var deleteButton: UIButton = {
        let button = UIButton()
        button.setTitle("Delete task", for: .normal)
        button.backgroundColor = .red
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(deleteTask), for: .touchUpInside)
        return button
    }()
    
    // MARK: - setup
    
    private func setup() {
        let viewController        = self
        let interactor            = DetailInteractor()
        let presenter             = DetailPresenter()
        let router                = DetailRouter()
        viewController.interactor = interactor
        viewController.router     = router
        interactor.presenter      = presenter
        presenter.viewController  = viewController
        router.viewController     = viewController
        router.dataStore          = interactor
    }
    
    // MARK: - lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        updateFieldsIfTaskExist()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "dots"), style: .done, target: self, action: #selector(settingsAction))
        
        view.addSubview(taskTextField)
        view.addSubview(descriptionTextView)
        view.addSubview(imageView)
        view.addSubview(saveButton)
        view.addSubview(deleteButton)
        
        setupConstraintsForTaskTextField()
        setupConstraintsForDescriptionTextView()
        setupConstraintsForImageView()
        setupConstraintsForSaveButton()
        setupConstraintsForDeleteButton()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissMyKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    // MARK: - methods
    
    @objc func dismissMyKeyboard() {
        view.endEditing(true)
    }
    
    
    func updateFieldsIfTaskExist() {
        let request = DetailModel.UpdateFieldsIfTaskExists.Request()
        interactor?.updateFieldsIfTaskExists(request: request)
    }
    
    
    func addTask() {
        guard taskTextField.text != "" else {
            let request = DetailModel.AddTask.Request(addTaskFields: nil, errorMessage: "Please enter a name of the task.")
            interactor?.addTask(request: request)
            return
        }
        
        guard descriptionTextView.text != "" else {
            let request = DetailModel.AddTask.Request(addTaskFields: nil, errorMessage: "Please enter a description of the task.")
            interactor?.addTask(request: request)
            return
        }
        
        guard let taskText = taskTextField.text,
              let descriptionText = descriptionTextView.text else { return }
        let taskFields = DetailModel.AddTaskFields(name: taskText, description: descriptionText, date: Date() as NSDate)
        let request = DetailModel.AddTask.Request(addTaskFields: taskFields, errorMessage: nil)
        interactor?.addTask(request: request)
    }
    
    
    func displayNewTaskAdded(viewModel: DetailModel.AddTask.ViewModel) {
        if viewModel.errorMessage != nil {
            let alertController = UIAlertController(title: "Error", message: viewModel.errorMessage, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            present(alertController, animated: true)
            return
        } else {
            self.showAlert(with: "Success", and: "The task is saved")
        }
    }
    
    
    func displayTaskDeleted(viewModel: DetailModel.DeleteTask.ViewModel) {
        if viewModel.errorMessage != nil {
            let alertController = UIAlertController(title: "Error", message: viewModel.errorMessage, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            present(alertController, animated: true)
            return
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func updateTask() {
        guard let nameText = taskTextField.text,
              let descriptionText = descriptionTextView.text else { return }
        let taskFields = DetailModel.AddTaskFields(name: nameText, description: descriptionText, date: Date() as NSDate)
        let request = DetailModel.UpdateTask.Request(task: taskFields)
        interactor?.updateTask(request: request)
    }
    
    
    func displayUpdateTask(viewModel: DetailModel.UpdateTask.ViewModel) {
        task = viewModel.task
        self.showAlert(with: "Success", and: "The task is saved")
    }
    
    
    func displayUpdateFieldsIfTaskExists(viewModel: DetailModel.UpdateFieldsIfTaskExists.ViewModel) {
        if viewModel.task != nil {
            guard let taskToUpdate = viewModel.task else { return }
            task = taskToUpdate
            self.taskTextField.text = taskToUpdate.taskName
            self.descriptionTextView.text = taskToUpdate.taskDescription
            self.imageView.image = UIImage(data: taskToUpdate.imageData as Data)
        }
    }
    
    
    func displayUpdateImage(viewModel: DetailModel.UpdateImage.ViewModel) {
        if viewModel.task != nil {
            guard let taskToUpdate = viewModel.task else { return }
            task = taskToUpdate
            self.imageView.image = UIImage(data: taskToUpdate.imageData as Data)
        }
    }
    
    
    @objc func saveTask() {
        task != nil ? updateTask(): addTask()
    }
    
}

// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate

extension DetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @objc func deleteTask() {
        if self.task != nil {
            let alert = UIAlertController(title: "Delete task", message: "Do you really want to delete this task?", preferredStyle: .actionSheet)
            let deleteAction = UIAlertAction(title: "Yes, delete", style: UIAlertAction.Style.default, handler: { (_)in
                let request = DetailModel.DeleteTask.Request(errorMessage: nil)
                self.interactor?.deleteTask(request: request)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.showAlert(with: "Unable to perform", and: "The task is not saved yet")
        }
    }
    
    
    @objc func settingsAction() {
        let alert = UIAlertController(title: "Settings", message: "Change or delete picture of your task", preferredStyle: .actionSheet)
        let changeImage = UIAlertAction(title: "Add/Change Image", style: UIAlertAction.Style.default, handler: { (_)in
            if self.task != nil {
                self.handlePlusPhoto()
            } else {
                self.showAlert(with: "Unable to perform", and: "To add image, please save the task")
            }
        })
        
        let removeImage = UIAlertAction(title: "Remove Image", style: UIAlertAction.Style.default, handler: { (_)in
            let request = DetailModel.UpdateImage.Request(imageData: NSData())
            self.interactor?.updateImage(request: request)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(changeImage)
        alert.addAction(removeImage)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func handlePlusPhoto() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            picker.dismiss(animated: true, completion: nil)
            return
        }
        
        if let dataImage = image.jpegData(compressionQuality: 1.0) {
            let request = DetailModel.UpdateImage.Request(imageData: dataImage as NSData)
            interactor?.updateImage(request: request)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
}

// MARK: - sizes extension

extension DetailViewController {
    
   
    func setupConstraintsForTaskTextField() {
        taskTextField.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(10)
            make.left.equalTo(view.snp.left).offset(10)
            make.right.equalTo(view.snp.right).offset(-10)
            make.height.equalTo(40)
        }
    }
    
    
    func setupConstraintsForDescriptionTextView() {
        descriptionTextView.snp.makeConstraints { make in
            make.top.equalTo(taskTextField.snp.bottom).offset(10)
            make.left.equalTo(view.snp.left).offset(10)
            make.right.equalTo(view.snp.right).offset(-10)
            make.height.equalTo(150)
        }
    }
    
    
    func setupConstraintsForImageView() {
        imageView.snp.makeConstraints { make in
            make.top.equalTo(descriptionTextView.snp.bottom).offset(10)
            make.left.equalTo(view.snp.left).offset(10)
            make.right.equalTo(view.snp.right).offset(-10)
            make.height.equalTo(200)
        }
    }
    
    
    func setupConstraintsForSaveButton() {
        saveButton.snp.makeConstraints { make in
            make.top.equalTo(imageView.snp.bottom).offset(10)
            make.left.equalTo(view.snp.left).offset(10)
            make.right.equalTo(view.snp.right).offset(-10)
            make.height.equalTo(40)
        }
    }
    
    
    func setupConstraintsForDeleteButton() {
        deleteButton.snp.makeConstraints { make in
            make.top.equalTo(saveButton.snp.bottom).offset(10)
            make.left.equalTo(view.snp.left).offset(10)
            make.right.equalTo(view.snp.right).offset(-10)
            make.height.equalTo(40)
        }
    }
    
    
}

