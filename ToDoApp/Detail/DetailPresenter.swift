//
//  DetailPresenter.swift
//  ToDoApp
//
//  Created by Роман Рачев on 04.02.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DetailPresenterLogic {
    func presentTaskAdded(response: DetailModel.AddTask.Response)
    func presentUpdateFieldsIfTaskExists(response: DetailModel.UpdateFieldsIfTaskExists.Response)
    func presentUpdateTask(response: DetailModel.UpdateTask.Response)
    func presentTaskDeleted(response: DetailModel.DeleteTask.Response)
    func presentUpdateImage(response: DetailModel.UpdateImage.Response)
}

class DetailPresenter: DetailPresenterLogic {
    
    // MARK: - properties
    
    weak var viewController: DetailDisplayLogic?
    
    // MARK: - methods
    
    func presentTaskAdded(response: DetailModel.AddTask.Response) {
        if response.errorMessage != nil {
            let viewModel = DetailModel.AddTask.ViewModel(task: nil, errorMessage: response.errorMessage)
            viewController?.displayNewTaskAdded(viewModel: viewModel)
            return
        }
        let taskAdded = response.task
        let viewModel = DetailModel.AddTask.ViewModel(task: taskAdded, errorMessage: nil)
        viewController?.displayNewTaskAdded(viewModel: viewModel)
    }
    
    
    func presentTaskDeleted(response: DetailModel.DeleteTask.Response) {
        if response.errorMessage != nil {
            let viewModel = DetailModel.DeleteTask.ViewModel(task: nil, errorMessage: response.errorMessage)
            viewController?.displayTaskDeleted(viewModel: viewModel)
            return
        }
        let taskDeleted = response.task
        let viewModel = DetailModel.DeleteTask.ViewModel(task: taskDeleted, errorMessage: nil)
        viewController?.displayTaskDeleted(viewModel: viewModel)
    }
    
    
    func presentUpdateFieldsIfTaskExists(response: DetailModel.UpdateFieldsIfTaskExists.Response) {
        if response.task != nil {
            let viewModel = DetailModel.UpdateFieldsIfTaskExists.ViewModel(task: response.task)
            viewController?.displayUpdateFieldsIfTaskExists(viewModel: viewModel)
        } else {
            let viewModel = DetailModel.UpdateFieldsIfTaskExists.ViewModel(task: nil)
            viewController?.displayUpdateFieldsIfTaskExists(viewModel: viewModel)
        }
    }
    
    
    func presentUpdateTask(response: DetailModel.UpdateTask.Response) {
        let taskToUpdate = response.task
        let viewModel = DetailModel.UpdateTask.ViewModel(task: taskToUpdate)
        viewController?.displayUpdateTask(viewModel: viewModel)
    }
    
    
    func presentUpdateImage(response: DetailModel.UpdateImage.Response) {
        let taskToUpdate = response.task
        let viewModel = DetailModel.UpdateImage.ViewModel(task: taskToUpdate)
        viewController?.displayUpdateImage(viewModel: viewModel)
    }
    
    
}
